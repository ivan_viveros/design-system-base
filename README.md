[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)
[![TypeScript](https://badges.frapsoft.com/typescript/version/typescript-next.svg?v=101)](https://github.com/ellerbrock/typescript-badges/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)


[![Yaydoo logo](https://gist.githubusercontent.com/viveralia/cc3c7e0d470f14cfc5d5612b2afffa44/raw/76d68629c37a5677760f2e410f6235753c4b0845/yaydoo.svg)](https://yaydoo.com/)

# Yaydoo Design System

This project aims to align the look and feel across all of the Yaydoo family products.

You can [find the Figma version here](https://www.figma.com/file/iDateKZsBqP6F9NSje3xXa/Design-System).

## Architecture
At this time, the project contains a monorepository with 2 main packages:

- `@yaydookit/react`: The component library for React

- `@yaydookit/theming`: The DS foundations in JSON format for CSS-in-JS

Feel free to extend the component packages for any frontend framework as required.

**Bonus:** There's also included a playground section to import and test your components before shipping them to NPM. 

### Components structure
The UI components are separated into folders based on [Brad Frost's Atomic Design Principles](https://bradfrost.com/blog/post/atomic-web-design/).

## Developing
Install the dependencies with yarn:

```sh
$ yarn
```

Run all the necessary packages in the monorepository with a single command:

```sh
$ yarn dev
```

Or navigate in each one and run them independently.

## Committing
After adding a feature or making any changes, it is recommended that you use the commitizen CLI to commit those changes.

```sh
$ yarn cm
```

It will prompt you some questions about the changes made and run a local pipeline to ensure code quality across the codebase. The CLI will fix most of the problems, but it is also likely that you fix some others.
