[![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components)

[![Yaydoo logo](https://gist.githubusercontent.com/viveralia/cc3c7e0d470f14cfc5d5612b2afffa44/raw/76d68629c37a5677760f2e410f6235753c4b0845/yaydoo.svg)](https://yaydoo.com/)

# React Components (Web)

Before you start developing, make sure to follow these guidelines:
- Unit test your component
- Document your component in storybook

After you are done, remember to add the pah to your component inside the `rollup.config.js`.