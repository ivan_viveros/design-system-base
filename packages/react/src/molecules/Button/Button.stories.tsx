import { ComponentMeta, ComponentStory } from "@storybook/react";
import * as React from "react";

import Button, { ButtonProps } from "./Button";

export default {
  title: "Molecules/Button",
  component: Button,
  argTypes: {
    label: {
      control: { type: "text" },
    },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Common = Template.bind({});
(Common.args as ButtonProps) = {
  label: "Hello World",
};
