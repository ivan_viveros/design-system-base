import { render } from "@testing-library/react";
import * as React from "react";

import Button from "./Button";

it("renders a label", () => {
  const { getByTestId } = render(<Button label="Hello world!" />);
  expect(getByTestId("yaydookit-button")).toBeInTheDocument();
});
