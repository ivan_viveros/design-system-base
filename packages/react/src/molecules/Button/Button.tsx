import * as React from "react";

export interface ButtonProps extends Pick<React.HTMLProps<HTMLButtonElement>, "onClick"> {
  label: string;
}

const Button: React.FC<ButtonProps> = ({ label, ...props }) => {
  return (
    <button data-testid="yaydookit-button" {...props}>
      {label}
    </button>
  );
};

export default Button;
