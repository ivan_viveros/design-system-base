import { Button } from "@yaydookit/react";
import * as React from "react";
import { render } from "react-dom";

// Play with the design system components here 👇
const App: React.FC = () => {
  const [count, setCount] = React.useState(0);

  const handleClick = () => {
    setCount((count) => count + 1);
  };

  return (
    <main>
      <h1>Count: {count}</h1>
      <Button label="Click me to start counting" onClick={handleClick} />
      <p>Btw, this button comes from the Design System</p>
    </main>
  );
};

render(<App />, document.querySelector("#root"));
